﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 24/04/2020 19:08:33
-- Server version: 5.5.5-10.1.40-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE lasrecetucas;

--
-- Drop table `comentan`
--
DROP TABLE IF EXISTS comentan;

--
-- Drop table `comentarios`
--
DROP TABLE IF EXISTS comentarios;

--
-- Drop table `consultan`
--
DROP TABLE IF EXISTS consultan;

--
-- Drop table `usuarios`
--
DROP TABLE IF EXISTS usuarios;

--
-- Drop table `contienen`
--
DROP TABLE IF EXISTS contienen;

--
-- Drop table `ingredientes`
--
DROP TABLE IF EXISTS ingredientes;

--
-- Drop table `dificultad`
--
DROP TABLE IF EXISTS dificultad;

--
-- Drop table `pasos`
--
DROP TABLE IF EXISTS pasos;

--
-- Drop table `recetas`
--
DROP TABLE IF EXISTS recetas;

--
-- Drop table `categorias`
--
DROP TABLE IF EXISTS categorias;

--
-- Set default database
--
USE lasrecetucas;

--
-- Create table `categorias`
--
CREATE TABLE categorias (
  id_categorias int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(20) DEFAULT NULL,
  PRIMARY KEY (id_categorias)
)
ENGINE = INNODB,
AUTO_INCREMENT = 13,
AVG_ROW_LENGTH = 1365,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `recetas`
--
CREATE TABLE recetas (
  id_recetas int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(50) DEFAULT NULL,
  duracion varchar(20) DEFAULT NULL,
  id_categorias int(11) DEFAULT NULL,
  PRIMARY KEY (id_recetas)
)
ENGINE = INNODB,
AUTO_INCREMENT = 13,
AVG_ROW_LENGTH = 1365,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE recetas
ADD CONSTRAINT fk_recetas_categorias FOREIGN KEY (id_categorias)
REFERENCES categorias (id_categorias);

--
-- Create table `pasos`
--
CREATE TABLE pasos (
  id_pasos int(11) NOT NULL AUTO_INCREMENT,
  numero_pasos int(20) DEFAULT NULL,
  descripcion varchar(5000) DEFAULT NULL,
  id_recetas int(11) DEFAULT NULL,
  PRIMARY KEY (id_pasos)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE pasos
ADD CONSTRAINT fk_recetas_pasos FOREIGN KEY (id_recetas)
REFERENCES recetas (id_recetas);

--
-- Create table `dificultad`
--
CREATE TABLE dificultad (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_recetas int(11) DEFAULT NULL,
  dificultad varchar(20) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 3,
AVG_ROW_LENGTH = 8192,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE dificultad
ADD CONSTRAINT fk_recetas_dificultad FOREIGN KEY (id)
REFERENCES recetas (id_recetas);

--
-- Create table `ingredientes`
--
CREATE TABLE ingredientes (
  id_ingredientes int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(50) DEFAULT NULL,
  PRIMARY KEY (id_ingredientes)
)
ENGINE = INNODB,
AUTO_INCREMENT = 34,
AVG_ROW_LENGTH = 496,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `contienen`
--
CREATE TABLE contienen (
  id_contienen int(11) NOT NULL AUTO_INCREMENT,
  id_ingredientes int(11) DEFAULT NULL,
  id_recetas int(11) DEFAULT NULL,
  cantidad varchar(50) DEFAULT NULL,
  PRIMARY KEY (id_contienen)
)
ENGINE = INNODB,
AUTO_INCREMENT = 60,
AVG_ROW_LENGTH = 277,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `uk_contienen` on table `contienen`
--
ALTER TABLE contienen
ADD UNIQUE INDEX uk_contienen (id_ingredientes, id_recetas);

--
-- Create foreign key
--
ALTER TABLE contienen
ADD CONSTRAINT fk_contienen_pasos FOREIGN KEY (id_ingredientes)
REFERENCES ingredientes (id_ingredientes);

--
-- Create foreign key
--
ALTER TABLE contienen
ADD CONSTRAINT fk_recetas_contienen FOREIGN KEY (id_recetas)
REFERENCES recetas (id_recetas);

--
-- Create table `usuarios`
--
CREATE TABLE usuarios (
  id_usuarios int(11) NOT NULL AUTO_INCREMENT,
  nombre_usuario varchar(20) DEFAULT NULL,
  email varchar(30) DEFAULT NULL,
  nombre varchar(20) DEFAULT NULL,
  apellidos varchar(40) DEFAULT NULL,
  fecha_nacimiento date DEFAULT NULL,
  PRIMARY KEY (id_usuarios)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `consultan`
--
CREATE TABLE consultan (
  id_consultan int(11) NOT NULL AUTO_INCREMENT,
  id_usuarios int(11) DEFAULT NULL,
  id_recetas int(11) DEFAULT NULL,
  PRIMARY KEY (id_consultan)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `uk_consultan` on table `consultan`
--
ALTER TABLE consultan
ADD UNIQUE INDEX uk_consultan (id_usuarios, id_recetas);

--
-- Create foreign key
--
ALTER TABLE consultan
ADD CONSTRAINT fk_consultan_recetas FOREIGN KEY (id_recetas)
REFERENCES recetas (id_recetas);

--
-- Create foreign key
--
ALTER TABLE consultan
ADD CONSTRAINT fk_consultan_usuarios FOREIGN KEY (id_usuarios)
REFERENCES usuarios (id_usuarios);

--
-- Create table `comentarios`
--
CREATE TABLE comentarios (
  id_comentarios int(11) NOT NULL AUTO_INCREMENT,
  likes int(11) DEFAULT NULL,
  descripcion varchar(500) DEFAULT NULL,
  PRIMARY KEY (id_comentarios)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `comentan`
--
CREATE TABLE comentan (
  id_comentarios int(11) NOT NULL AUTO_INCREMENT,
  id_usuarios int(11) DEFAULT NULL,
  id_recetas int(11) DEFAULT NULL,
  PRIMARY KEY (id_comentarios),
  UNIQUE INDEX uk_comentan_recetas (id_recetas, id_comentarios),
  UNIQUE INDEX uk_comentan_recetas_comentarios_usuarios (id_usuarios, id_recetas, id_comentarios)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `uk_comentan_usuarios` on table `comentan`
--
ALTER TABLE comentan
ADD UNIQUE INDEX uk_comentan_usuarios (id_recetas, id_usuarios);

--
-- Create foreign key
--
ALTER TABLE comentan
ADD CONSTRAINT fk_comentan_comentarios FOREIGN KEY (id_comentarios)
REFERENCES comentarios (id_comentarios);

--
-- Create foreign key
--
ALTER TABLE comentan
ADD CONSTRAINT fk_comentan_recetas FOREIGN KEY (id_recetas)
REFERENCES recetas (id_recetas);

--
-- Create foreign key
--
ALTER TABLE comentan
ADD CONSTRAINT fk_comentan_usuarios FOREIGN KEY (id_usuarios)
REFERENCES usuarios (id_usuarios);

-- 
-- Dumping data for table categorias
--
INSERT INTO categorias VALUES
(1, 'Postres'),
(2, 'Arroces'),
(3, 'Picoteo'),
(4, 'Carnes'),
(5, 'Cremas y Sopas'),
(6, 'Ensaladas'),
(7, 'Huevos'),
(8, 'Pasta y Pizza'),
(9, 'Pescado y Marisco'),
(10, 'Potaje y Legumbre'),
(11, 'Setas y Hongos'),
(12, 'Verdura y Hortalizas');

-- 
-- Dumping data for table ingredientes
--
INSERT INTO ingredientes VALUES
(1, 'Huevos'),
(2, 'Pimiento Rojo'),
(3, 'Pimiento Verde'),
(4, 'Zanahoria'),
(5, 'Cebolla'),
(6, 'Champiñones'),
(7, 'Tomate'),
(8, 'Aceite de Oliva'),
(9, 'Sal'),
(10, 'Leche'),
(11, 'Mantequilla'),
(12, 'Cebolleta'),
(13, 'Harina'),
(14, 'Huevos Cocidos'),
(15, 'Jamón Ibérico'),
(16, 'Arroz para Risotto'),
(17, 'Setas Variadas'),
(18, 'Vino Blanco'),
(19, 'Ajo'),
(20, 'Queso Parmigiano'),
(21, 'Pimienta'),
(22, 'Caldo de Verduras'),
(23, 'Carne de Ternera Picada'),
(24, 'Nuez Moscada'),
(25, 'Perejil'),
(26, 'Pan de Molde'),
(27, 'Puerro'),
(28, 'Calabacín'),
(29, 'Hoja de Laurel'),
(30, 'Patata'),
(31, 'Nata para Cocinar'),
(32, 'Cebollino'),
(33, 'Pimienta Blanca');

-- 
-- Dumping data for table usuarios
--
INSERT INTO usuarios VALUES
(1, 'Frank_g0', 'fragosu@gmail.com', 'Francisco', 'Gómez Suárez', '1985-03-21');

-- 
-- Dumping data for table recetas
--
INSERT INTO recetas VALUES
(1, 'Tortilla de Verduras', '25 minutos', 7),
(2, 'Croquetas de Jamón Ibérico', '3 horas', 3),
(3, 'Risotto de Setas ', '40 minutos', 2),
(4, 'Albóndigas de Ternera', '90 minutos', 4),
(5, 'Crema de Puerros', '35 minutos', 5),
(6, 'Ensalada Waldorf', '20 minutos', 6),
(7, 'Espaguetis de Marisco', '50 minutos', 8),
(8, 'Lubina al Horno', '60 minutos', 9),
(9, 'Tarta de Queso al Horno', '60 minutos', 1),
(10, 'Cocido Montañés', '120 minutos', 10),
(11, 'Chipirones Rellenos Encebollados', '45 minutos', 11),
(12, 'Calabacín Relleno de Verduras y Salmón', '30 minutos', 12);

-- 
-- Dumping data for table comentarios
--
-- Table lasrecetucas.comentarios does not contain any data (it is empty)

-- 
-- Dumping data for table pasos
--
-- Table lasrecetucas.pasos does not contain any data (it is empty)

-- 
-- Dumping data for table dificultad
--
INSERT INTO dificultad VALUES
(1, 1, 'Media-fácil'),
(2, 2, 'Media');

-- 
-- Dumping data for table contienen
--
INSERT INTO contienen VALUES
(1, 1, 1, '6 ud.'),
(2, 2, 1, '50 gr.'),
(3, 3, 1, '50 gr.'),
(4, 4, 1, '1 ud.'),
(5, 5, 1, '1/2 ud.'),
(6, 6, 1, '100 gr.'),
(7, 7, 1, '1 ud.'),
(8, 8, 1, '15 ml.'),
(9, 9, 1, '5 gr.'),
(10, 10, 2, '3 l.'),
(11, 11, 2, '80 gr.'),
(12, 12, 2, '100 gr.'),
(13, 13, 2, '225 gr.'),
(14, 14, 2, '3 ud.'),
(15, 15, 2, '300 gr.'),
(16, 8, 2, '80 ml.'),
(17, 9, 2, '5 gr.'),
(18, 16, 3, '320 gr.'),
(19, 17, 3, '400 gr.'),
(20, 5, 3, '1 ud.'),
(21, 11, 3, '40 gr.'),
(22, 18, 3, '250 ml.'),
(23, 19, 3, '1 diente'),
(24, 20, 3, '100 gr.'),
(25, 8, 3, '50 ml.'),
(26, 9, 3, '5 gr.'),
(27, 21, 2, '5 gr.'),
(28, 22, 3, '1 l.'),
(29, 23, 4, '400 gr.'),
(30, 19, 4, '2 dientes'),
(31, 5, 4, '1 ud.'),
(32, 11, 4, '15 gr.'),
(33, 1, 4, '1 ud.'),
(34, 9, 4, '5 gr.'),
(35, 24, 4, '2 gr.'),
(36, 25, 4, '15 gr.'),
(37, 13, 4, '50 gr.'),
(38, 26, 4, '1 ud.'),
(39, 10, 4, '10 ml.'),
(40, 27, 4, '1 ud.'),
(41, 2, 4, '50 gr.'),
(42, 3, 4, '50 gr.'),
(43, 4, 4, '1 ud.'),
(44, 7, 4, '1 ud.'),
(45, 28, 4, '100 gr.'),
(46, 18, 4, '150 ml.'),
(47, 21, 4, '5 gr.'),
(48, 8, 4, '15 ml.'),
(49, 29, 4, '1 ud.'),
(50, 27, 5, '500 gr.'),
(51, 30, 5, '250 gr.'),
(52, 22, 5, '1 l.'),
(53, 31, 5, '150 ml.'),
(54, 11, 5, '100 gr.'),
(55, 8, 5, '15 ml.'),
(56, 32, 5, '10 gr. (decorar)'),
(57, 15, 5, '10 gr. (decorar)'),
(58, 9, 5, '5 gr.'),
(59, 33, 5, '5 gr.');

-- 
-- Dumping data for table consultan
--
-- Table lasrecetucas.consultan does not contain any data (it is empty)

-- 
-- Dumping data for table comentan
--
-- Table lasrecetucas.comentan does not contain any data (it is empty)

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;