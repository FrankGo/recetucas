﻿/* creación de la base de datos */

DROP DATABASE IF EXISTS Las_recetucas;
CREATE DATABASE Las_recetucas;

USE Las_recetucas;


/* crear las tablas */

  -- usuarios
  CREATE OR REPLACE TABLE usuarios(
    id_usuarios int AUTO_INCREMENT,
    nombre_usuario varchar(20),
    email varchar(30),
    nombre varchar(20),
    apellidos varchar(40),
    fecha_nacimiento date,
    PRIMARY KEY (id_usuarios)
  );

  -- ingredientes
CREATE OR REPLACE TABLE ingredientes(
    id_ingredientes int AUTO_INCREMENT,
    nombre varchar(20),
    PRIMARY KEY (id_ingredientes)
    );

  -- categorías
CREATE OR REPLACE TABLE categorias(
    id_categorias int AUTO_INCREMENT,
    nombre varchar(20),
    PRIMARY KEY (id_categorias)
    );

  -- pasos
  CREATE OR REPLACE TABLE pasos(
   id_pasos int AUTO_INCREMENT,
   numero_pasos int(20),
   descripcion varchar(500),
   id_recetas int,
    PRIMARY KEY (id_pasos)
  );

  -- recetas
 CREATE OR REPLACE TABLE recetas(
    id_recetas int AUTO_INCREMENT,
    nombre varchar(20),
    duracion varchar (10),
    id_categorias int,
    PRIMARY KEY (id_recetas)
  );

  -- comentarios
CREATE OR REPLACE TABLE comentarios(
    id_comentarios int AUTO_INCREMENT,
    likes int,
    descripcion varchar(500),
    PRIMARY KEY (id_comentarios)
    );

  -- contienen
 CREATE OR REPLACE TABLE contienen(
    id_contienen int AUTO_INCREMENT, 
    id_ingredientes int, 
    id_recetas int,
    PRIMARY KEY(id_contienen)
  );

  -- consultan
 CREATE OR REPLACE TABLE consultan(
    id_consultan int AUTO_INCREMENT, 
    id_usuarios int,
    id_recetas int,
    PRIMARY KEY(id_consultan)
  );

  -- comentan
 CREATE OR REPLACE TABLE comentan(
    id_comentarios int AUTO_INCREMENT, 
    id_usuarios int,
    id_recetas int,
    PRIMARY KEY(id_comentarios)
);

  -- dificultad
 CREATE OR REPLACE TABLE dificultad(
    id int AUTO_INCREMENT,
    id_recetas int,
    dificultad varchar (20),
    PRIMARY KEY(id)
);

/** creacion de las restricciones **/

ALTER TABLE recetas
  ADD CONSTRAINT fk_recetas_categorias
  FOREIGN KEY (id_categorias)
  REFERENCES categorias (id_categorias);


ALTER TABLE pasos
  ADD CONSTRAINT fk_recetas_pasos
  FOREIGN KEY (id_recetas)
  REFERENCES recetas (id_recetas);


ALTER TABLE contienen
  ADD CONSTRAINT fk_contienen_pasos
  FOREIGN KEY (id_ingredientes)
  REFERENCES ingredientes (id_ingredientes),


  ADD CONSTRAINT fk_recetas_contienen
  FOREIGN KEY (id_recetas)
  REFERENCES recetas (id_recetas),

  ADD CONSTRAINT uk_contienen
  UNIQUE KEY (id_ingredientes, id_recetas);



ALTER TABLE dificultad
  ADD CONSTRAINT fk_recetas_dificultad
  FOREIGN KEY (id)
  REFERENCES recetas (id_recetas);


ALTER TABLE consultan
  ADD CONSTRAINT fk_consultan_usuarios
  FOREIGN KEY (id_usuarios)
  REFERENCES usuarios (id_usuarios),

  ADD CONSTRAINT fk_consultan_recetas
  FOREIGN KEY (id_recetas)
  REFERENCES recetas (id_recetas),

  ADD CONSTRAINT uk_consultan
  UNIQUE KEY (id_usuarios, id_recetas);



ALTER TABLE comentan
  ADD CONSTRAINT fk_comentan_usuarios
  FOREIGN KEY (id_usuarios)
  REFERENCES usuarios (id_usuarios),

  ADD CONSTRAINT fk_comentan_recetas
  FOREIGN KEY (id_recetas)
  REFERENCES recetas (id_recetas),

  ADD CONSTRAINT fk_comentan_comentarios
  FOREIGN KEY (id_comentarios)
  REFERENCES comentarios (id_comentarios),

  ADD CONSTRAINT uk_comentan_recetas
  UNIQUE KEY (id_recetas, id_comentarios),

  ADD CONSTRAINT uk_comentan_usuarios
  UNIQUE KEY (id_recetas, id_usuarios),

  ADD CONSTRAINT uk_comentan_recetas_comentarios_usuarios
  UNIQUE KEY (id_usuarios, id_recetas, id_comentarios);
 