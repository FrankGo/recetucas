<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dificultad".
 *
 * @property int $id
 * @property int|null $id_recetas
 * @property string|null $dificultad
 *
 * @property Recetas $id0
 */
class Dificultad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dificultad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_recetas'], 'integer'],
            [['dificultad'], 'string', 'max' => 20],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Recetas::className(), 'targetAttribute' => ['id' => 'id_recetas']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_recetas' => 'Id Recetas',
            'dificultad' => 'Dificultad',
        ];
    }

    /**
     * Gets query for [[Id0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Recetas::className(), ['id_recetas' => 'id']);
    }
}
