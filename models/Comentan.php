<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comentan".
 *
 * @property int $id_comentarios
 * @property int|null $id_usuarios
 * @property int|null $id_recetas
 *
 * @property Comentarios $comentarios
 * @property Recetas $recetas
 * @property Usuarios $usuarios
 */
class Comentan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comentan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuarios', 'id_recetas'], 'integer'],
            [['id_recetas', 'id_usuarios'], 'unique', 'targetAttribute' => ['id_recetas', 'id_usuarios']],
            [['id_comentarios'], 'exist', 'skipOnError' => true, 'targetClass' => Comentarios::className(), 'targetAttribute' => ['id_comentarios' => 'id_comentarios']],
            [['id_recetas'], 'exist', 'skipOnError' => true, 'targetClass' => Recetas::className(), 'targetAttribute' => ['id_recetas' => 'id_recetas']],
            [['id_usuarios'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_usuarios' => 'id_usuarios']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_comentarios' => 'Id Comentarios',
            'id_usuarios' => 'Id Usuarios',
            'id_recetas' => 'Id Recetas',
        ];
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasOne(Comentarios::className(), ['id_comentarios' => 'id_comentarios']);
    }

    /**
     * Gets query for [[Recetas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecetas()
    {
        return $this->hasOne(Recetas::className(), ['id_recetas' => 'id_recetas']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasOne(Usuarios::className(), ['id_usuarios' => 'id_usuarios']);
    }
}
