<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contienen".
 *
 * @property int $id_contienen
 * @property int|null $id_ingredientes
 * @property int|null $id_recetas
 * @property string|null $cantidad
 *
 * @property Ingredientes $ingredientes
 * @property Recetas $recetas
 */
class Contienen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contienen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_ingredientes', 'id_recetas'], 'integer'],
            [['cantidad'], 'string', 'max' => 50],
            [['id_ingredientes', 'id_recetas'], 'unique', 'targetAttribute' => ['id_ingredientes', 'id_recetas']],
            [['id_ingredientes'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredientes::className(), 'targetAttribute' => ['id_ingredientes' => 'id_ingredientes']],
            [['id_recetas'], 'exist', 'skipOnError' => true, 'targetClass' => Recetas::className(), 'targetAttribute' => ['id_recetas' => 'id_recetas']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_contienen' => 'Id Contienen',
            'id_ingredientes' => 'Id Ingredientes',
            'id_recetas' => 'Id Recetas',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * Gets query for [[Ingredientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientes()
    {
        return $this->hasOne(Ingredientes::className(), ['id_ingredientes' => 'id_ingredientes']);
    }

    /**
     * Gets query for [[Recetas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecetas()
    {
        return $this->hasOne(Recetas::className(), ['id_recetas' => 'id_recetas']);
    }
}
