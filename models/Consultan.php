<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "consultan".
 *
 * @property int $id_consultan
 * @property int|null $id_usuarios
 * @property int|null $id_recetas
 *
 * @property Recetas $recetas
 * @property Usuarios $usuarios
 */
class Consultan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuarios', 'id_recetas'], 'integer'],
            [['id_usuarios', 'id_recetas'], 'unique', 'targetAttribute' => ['id_usuarios', 'id_recetas']],
            [['id_recetas'], 'exist', 'skipOnError' => true, 'targetClass' => Recetas::className(), 'targetAttribute' => ['id_recetas' => 'id_recetas']],
            [['id_usuarios'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_usuarios' => 'id_usuarios']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_consultan' => 'Id Consultan',
            'id_usuarios' => 'Id Usuarios',
            'id_recetas' => 'Id Recetas',
        ];
    }

    /**
     * Gets query for [[Recetas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecetas()
    {
        return $this->hasOne(Recetas::className(), ['id_recetas' => 'id_recetas']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasOne(Usuarios::className(), ['id_usuarios' => 'id_usuarios']);
    }
}
