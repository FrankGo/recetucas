<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recetas".
 *
 * @property int $id_recetas
 * @property string|null $nombre
 * @property string|null $duracion
 * @property int|null $id_categorias
 *
 * @property Comentan[] $comentans
 * @property Comentarios[] $comentarios
 * @property Usuarios[] $usuarios
 * @property Consultan[] $consultans
 * @property Usuarios[] $usuarios0
 * @property Contienen[] $contienens
 * @property Ingredientes[] $ingredientes
 * @property Dificultad $dificultad
 * @property Pasos[] $pasos
 * @property Categorias $categorias
 */
class Recetas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recetas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_categorias','nombre','duracion', 'id_categorias'], 'required'],
            [['id_categorias'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['duracion'], 'string', 'max' => 20],
            [['id_categorias'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['id_categorias' => 'id_categorias']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_recetas' => 'Id Recetas',
            'nombre' => 'Nombre',
            'duracion' => 'Duracion',
            'id_categorias' => 'Id Categorias',
        ];
    }

    /**
     * Gets query for [[Comentans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentans()
    {
        return $this->hasMany(Comentan::className(), ['id_recetas' => 'id_recetas']);
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasMany(Comentarios::className(), ['id_comentarios' => 'id_comentarios'])->viaTable('comentan', ['id_recetas' => 'id_recetas']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuarios::className(), ['id_usuarios' => 'id_usuarios'])->viaTable('comentan', ['id_recetas' => 'id_recetas']);
    }

    /**
     * Gets query for [[Consultans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConsultans()
    {
        return $this->hasMany(Consultan::className(), ['id_recetas' => 'id_recetas']);
    }

    /**
     * Gets query for [[Usuarios0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios0()
    {
        return $this->hasMany(Usuarios::className(), ['id_usuarios' => 'id_usuarios'])->viaTable('consultan', ['id_recetas' => 'id_recetas']);
    }

    /**
     * Gets query for [[Contienens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContienens()
    {
        return $this->hasMany(Contienen::className(), ['id_recetas' => 'id_recetas']);
    }

    /**
     * Gets query for [[Ingredientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientes()
    {
        return $this->hasMany(Ingredientes::className(), ['id_ingredientes' => 'id_ingredientes'])->viaTable('contienen', ['id_recetas' => 'id_recetas']);
    }

    /**
     * Gets query for [[Dificultad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDificultad()
    {
        return $this->hasOne(Dificultad::className(), ['id' => 'id_recetas']);
    }

    /**
     * Gets query for [[Pasos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPasos()
    {
        return $this->hasMany(Pasos::className(), ['id_recetas' => 'id_recetas']);
    }

    /**
     * Gets query for [[Categorias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategorias()
    {
        return $this->hasOne(Categorias::className(), ['id_categorias' => 'id_categorias']);
    }
}
