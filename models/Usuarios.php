<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id_usuarios
 * @property string|null $nombre_usuario
 * @property string|null $email
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $fecha_nacimiento
 *
 * @property Comentan[] $comentans
 * @property Recetas[] $recetas
 * @property Consultan[] $consultans
 * @property Recetas[] $recetas0
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_nacimiento'], 'safe'],
            [['nombre_usuario', 'nombre'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 30],
            [['apellidos'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_usuarios' => 'Id Usuarios',
            'nombre_usuario' => 'Nombre Usuario',
            'email' => 'Email',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'fecha_nacimiento' => 'Fecha Nacimiento',
        ];
    }

    /**
     * Gets query for [[Comentans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentans()
    {
        return $this->hasMany(Comentan::className(), ['id_usuarios' => 'id_usuarios']);
    }

    /**
     * Gets query for [[Recetas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecetas()
    {
        return $this->hasMany(Recetas::className(), ['id_recetas' => 'id_recetas'])->viaTable('comentan', ['id_usuarios' => 'id_usuarios']);
    }

    /**
     * Gets query for [[Consultans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConsultans()
    {
        return $this->hasMany(Consultan::className(), ['id_usuarios' => 'id_usuarios']);
    }

    /**
     * Gets query for [[Recetas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecetas0()
    {
        return $this->hasMany(Recetas::className(), ['id_recetas' => 'id_recetas'])->viaTable('consultan', ['id_usuarios' => 'id_usuarios']);
    }
}
