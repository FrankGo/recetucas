<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ingredientes".
 *
 * @property int $id_ingredientes
 * @property string|null $nombre
 *
 * @property Contienen[] $contienens
 * @property Recetas[] $recetas
 */
class Ingredientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingredientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_ingredientes' => 'Id Ingredientes',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Contienens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContienens()
    {
        return $this->hasMany(Contienen::className(), ['id_ingredientes' => 'id_ingredientes']);
    }

    /**
     * Gets query for [[Recetas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecetas()
    {
        return $this->hasMany(Recetas::className(), ['id_recetas' => 'id_recetas'])->viaTable('contienen', ['id_ingredientes' => 'id_ingredientes']);
    }
}
