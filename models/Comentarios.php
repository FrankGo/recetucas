<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comentarios".
 *
 * @property int $id_comentarios
 * @property int|null $likes
 * @property string|null $descripcion
 *
 * @property Comentan $comentan
 * @property Recetas[] $recetas
 */
class Comentarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comentarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['likes'], 'integer'],
            [['descripcion'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_comentarios' => 'Id Comentarios',
            'likes' => 'Likes',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Comentan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentan()
    {
        return $this->hasOne(Comentan::className(), ['id_comentarios' => 'id_comentarios']);
    }

    /**
     * Gets query for [[Recetas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecetas()
    {
        return $this->hasMany(Recetas::className(), ['id_recetas' => 'id_recetas'])->viaTable('comentan', ['id_comentarios' => 'id_comentarios']);
    }
}
