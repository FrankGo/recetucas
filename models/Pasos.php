<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasos".
 *
 * @property int $id_pasos
 * @property int|null $numero_pasos
 * @property string|null $descripcion
 * @property int|null $id_recetas
 *
 * @property Recetas $recetas
 */
class Pasos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero_pasos', 'id_recetas'], 'integer'],
            [['descripcion'], 'string', 'max' => 5000],
            [['id_recetas'], 'exist', 'skipOnError' => true, 'targetClass' => Recetas::className(), 'targetAttribute' => ['id_recetas' => 'id_recetas']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pasos' => 'Id Pasos',
            'numero_pasos' => 'Numero Pasos',
            'descripcion' => 'Descripcion',
            'id_recetas' => 'Id Recetas',
        ];
    }

    /**
     * Gets query for [[Recetas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecetas()
    {
        return $this->hasOne(Recetas::className(), ['id_recetas' => 'id_recetas']);
    }
}
