<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contienen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contienen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_ingredientes')->textInput() ?>

    <?= $form->field($model, 'id_recetas')->textInput() ?>

    <?= $form->field($model, 'cantidad')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
