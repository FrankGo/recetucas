<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contienen */

$this->title = 'Cargar Contienen: ' . $model->id_contienen;
$this->params['breadcrumbs'][] = ['label' => 'Contienen', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_contienen, 'url' => ['view', 'id' => $model->id_contienen]];
$this->params['breadcrumbs'][] = 'Cargar';
?>
<div class="contienen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
