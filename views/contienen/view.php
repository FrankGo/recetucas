<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contienen */

$this->title = $model->id_contienen;
$this->params['breadcrumbs'][] = ['label' => 'Contienen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="contienen-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Cargar', ['Update', 'id' => $model->id_contienen], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['Delete', 'id' => $model->id_contienen], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_contienen',
            'id_ingredientes',
            'id_recetas',
            'cantidad',
        ],
    ]) ?>

</div>
