<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contienen';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contienen-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Contienen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
          /*  ['class' => 'yii\grid\SerialColumn'], */
          
//            'id_contienen',
//            'id_ingredientes',
            
            [
                'attribute' =>'Nombre de la Receta',
                'value' => 'recetas.nombre',
                ],
            [
                'attribute' =>'Nombre del Ingrediente',
                'value' => 'ingredientes.nombre',
                ],
            
//            'id_recetas',
            'cantidad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
