<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contienen */

$this->title = 'Crear Contienen';
$this->params['breadcrumbs'][] = ['label' => 'Contienen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contienen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
