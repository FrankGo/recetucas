<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consultan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Consultan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
           /* ['class' => 'yii\grid\SerialColumn'],*/

            'id_consultan',
            'id_usuarios',
            'id_recetas',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
