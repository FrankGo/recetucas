<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Consultan */

$this->title = 'Crear Consultan';
$this->params['breadcrumbs'][] = ['label' => 'Consultan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consultan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
