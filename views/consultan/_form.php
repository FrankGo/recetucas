<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Consultan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="consultan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_usuarios')->textInput() ?>

    <?= $form->field($model, 'id_recetas')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
