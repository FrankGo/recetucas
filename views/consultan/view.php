<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Consultan */

$this->title = $model->id_consultan;
$this->params['breadcrumbs'][] = ['label' => 'Consultan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="consultan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['Update', 'id' => $model->id_consultan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['Delete', 'id' => $model->id_consultan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_consultan',
            'id_usuarios',
            'id_recetas',
        ],
    ]) ?>

</div>
