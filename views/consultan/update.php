<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Consultan */

$this->title = 'Cargar Consultan: ' . $model->id_consultan;
$this->params['breadcrumbs'][] = ['label' => 'Consultan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_consultan, 'url' => ['view', 'id' => $model->id_consultan]];
$this->params['breadcrumbs'][] = 'Cargar';
?>
<div class="consultan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
