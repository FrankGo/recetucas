<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dificultad';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dificultad-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Dificultad', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'layout' =>"\n{items}",
        'dataProvider' => $dataProvider,
        'columns' => [
          /*  ['class' => 'yii\grid\SerialColumn'], */

//            'id',
//            'id_recetas',
                [
                'attribute' =>'Nombre de la Receta',
                'value' => 'id0.nombre',
                ],
            'dificultad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
