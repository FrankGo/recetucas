<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dificultad */

$this->title = 'Crear Dificultad';
$this->params['breadcrumbs'][] = ['label' => 'Dificultad', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dificultad-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
