<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pasos */

$this->title = 'Update Pasos: ' . $model->id_pasos;
$this->params['breadcrumbs'][] = ['label' => 'Pasos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pasos, 'url' => ['view', 'id' => $model->id_pasos]];
$this->params['breadcrumbs'][] = 'Cargar';
?>
<div class="pasos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
