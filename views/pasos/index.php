<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pasos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pasos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Pasos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'layout' =>"\n{items}",
        'dataProvider' => $dataProvider,
        'columns' => [
          /*  ['class' => 'yii\grid\SerialColumn'], */

            'id_recetas',[
                'attribute' =>'Nombre de la Receta',
                'value' => 'recetas.nombre',
                ],
//            'id_pasos',
            'numero_pasos',
            'descripcion',
            
               

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
