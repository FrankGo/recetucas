<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Setas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <center><h1><?= Html::encode($this->title) ?></h1></center>
    <br></br>
  <center><?= Html::img('@web/../img/setas1.png', ['alt' => '']) ?></center>
   <br></br>
     <div class="row">
            <div class="col-lg-12">
La temporada de setas se estrena en los bosques de media España con la recogida de las principales especies, que verán su valor aumentado en las verdulerías dada su condición de exquisitez gastronómica, muy apreciada en algunas zonas del país. 

Pero más allá de ser ingrediente de algunos de los platos más nobles, las setas tienen un alto valor nutricional y para la salud por las distinas sustancias y componentes que aportan y por los efectos que tienen sobre nuestro metabolismo. A continuación te reportamos diez beneficios de comer setas, si bien los mismos se dan más en el consumo de las setas salvajes que en las cultivadas industrialmente. 
<br></br>
1. Aportan fibra insoluble
En efecto, la lignina es uno de los principales ingredientes de las setas, con 2,5 gramos por cada 100 gramos, donde la mayor parte es agua. Se trata de una fibra leñosa, de relativo valor biológico de cara a la microbiota pero muy buena como fibra de arrastre para limpiar el tracto intestinal y también por su capacidad de hinchar se y dar consistencia al bolo. Para saber más te recomendamos: Ocho razones para reintroducir en nuestra dieta la fibra insoluble.
<br></br>
2. Tienen un alto poder saciante
Precisamente esta capacidad de la lignina de retener agua e hincharse en el estómago, aumenta la sensación de estómago lleno tras la ingesta de setas y hace que no queramos comer tanto, que seamos mas frugales y controlemos así la ingesta calórica previniendo la obesidad. Sobre el poder saciante te recomendamos: Siete alimentos para vigilar el peso sin recurrir a dietas milagro.
<br></br>
3. Contienen elementos antioxidantes
Un estudio de 2007 publicado en Journal of Nutrition destacaba el papel del aminoácido ergoteína, presente en la proteína de las setas, como un potente antioxidante con gran presencia en el champiñón silvestre, aunque no tanto en el industrial. El estudio destacaba además que dicho papel le confería un interesante papel preventivo ante distintos tipos de tumores. 
<br></br>
4. Tienen un bajo poder calórico
Como ya se ha explicado, su mayor porcentaje es agua y apenas suman 4 gramos por cada 100 de hidratos de carbono. Su poder calórico son 25 Kcal, lo que las hace un perfecto acompañamiento para todo tipo de platos o incluso solas, cuando hemos superado la ingesta diaria recomendada calórica pero tenemos hambre.
  <br></br>
  <center><?= Html::img('@web/../img/setas2.png', ['alt' => '']) ?></center>
   <br></br>
5. Aportan vitaminas y minerales imprescindibles
Según la Fundación Española del Corazón, que las recomienda como pieza clave en la lucha contra las enfermedades cardiovasculares, las setas son ricas en hierro, fósforo, yodo, magnesio, selenio, calcio, potasio, zinc, además de vitamina A y vitaminas del grupo B (concretamente B1, B2, B3), así como C y vitamina D que autosintetiza con la luz solar, al igual que los humanos. Aunque su versión, la D3, parece no ser tan eficaz como la D2, si puede ayudar. Destaca también por su bajo aporte en sodio, por lo que tiene incidencia positiva sobre la presión arterial, aunque esta desaparece al salarlas.
<br></br>
6. Pueden aportar los aminoácidos esenciales
Al ser una vía evolutiva intermedia entre los vegetales y los animales, su proteína está más cercana a la de estos últimos, la nuestra, y por lo tanto se considera de superior valor biológico, ya que en algunos géneros como es el caso de Pleurotus sp (el de la seta de ostra), se pueden encontrar todos los aminoácidos esenciales, según un estudio. 
<br></br>
7. Pueden ser un sustituto de la carne
Tanto por su proteína, que aunque no es muy abundante es de alta calidad, como por su contenido en aminoácidos esenciales, y también por su sabor umami, las setas pueden ayudarnos a reducir la ingesta semanal de carne roja. Te recomendamos el artículo Alimentos que nos pueden ayudar a decir adiós a la carne.
<br></br>
8. Pueden mejorar la diversidad de la microbiota
Hay varios estudios (este y este) que avalan que la lignina de las setas puede ayudar a mejorar la diversidad de la flora intestinal favoreciendo a ciertas bacterias que intervendrían en la reducción de la obesidad.
<br></br>
9. Aportan compuestos antiinflamatorios
Algunos de los hidratos de carbono de ciertas setas son ricos en betaglucanos, un polisacárido al que esta revisión le otorga grandes beneficios como antiinflamatorio y desarrollador del sistema inmunitario. Esta publicación también incide en el mismo sentido.
<br></br>
10. Ayudan a reducir el colesterol malo
Algunas setas son ricas en ácido linolénico conjugado, un ácido graso omega-6 que ayuda a reducir el colesterol metabólico malo o LDL. Además, sus cantidades son moderadas en relación a los suplementos de este ácido, que se han calificado de peligrosos tras algunos estudios en ratones.

     <br></br>
<center><?= Html::img('@web/../img/setas3.png', ['alt' => '']) ?></center>
       <br></br>
       
       <center><?= Html::a('Recetas con Setas', ['site/recetasporcategorias','id' => 7], ['class' => 'btn btn-default']) ?></center>
       
            </div>
     

          
    
</div>
<br></br>
     <div class="footer">
        <p>Footer</p>
    </div>
    
     <style>
  
        .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: black;
        color: white;
        text-align: center;
        }
        </style>
</div>
