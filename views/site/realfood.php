<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Real Food';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <center><h1><?= Html::encode($this->title) ?></h1></center>
 <br></br>
  <center><?= Html::img('@web/../img/realfood1.png', ['alt' => '']) ?></center>
 <br></br>
     <div class="row">
            <div class="col-lg-12">
                </br><br>
Real Food es un movimiento y estilo de vida que apuesta por comer alimentos
lo menos procesados posible, o lo que es lo mismo, comer “comida real”.
El principal objetivo del movimiento Real Food no es otro que mejorar la salud
de la población a través de su alimentación. Partiendo de esa premisa que
identifica qué es Real Food, ahondemos en el tema para entenderlo un poco
mejor…
<br></br>
La Real Food se entiende como los alimentos no procesados o cuyo
procesamiento industrial no haya perjudicado a la calidad de su composición o
empeorado sus propiedades presentes de manera natural. En resumidas
cuentas, se puede decir que la comida real es la compuesta por estos
ingredientes:
<br></br>
Las frutas, los frutos secos, las verduras y las hortalizas.
Las carnes no procesadas.
El pescado y marisco.
Las legumbres y los tubérculos.
Los huevos.
La leche fresca.
Los cereales de grano entero o 100% integrales.
Las infusiones y el café.
Las semillas, hierbas y especias.
<br></br>
  <center><?= Html::img('@web/../img/realfood2.png', ['alt' => '']) ?></center>
  <br></br>
Hay una serie de alimentos cuyo procesamiento, bien sea artesanal o industrial,
es beneficioso o inocuo en relación a sus propiedades. Estos son los alimentos
procesados que podemos considerar aptos dentro de una dieta saludable. Es
decir, son el resultado de al menos uno de estos dos procesos:
<br></br>
La retirada o adición de algunos ingredientes a los alimento enteros.
Un procesamiento industrial que los hace más duraderos, seguros y agradables
al paladar o prácticos para facilitar su consumo.
<br></br>
Suelen estar envasados y llevar su correspondiente etiquetado de alimentos
donde figuran a lo sumo 5 ingredientes, de los que como mucho un 10% son
harina refinada, azúcar o aceite vegetal refinado.
<br></br>
La comida real congelada o envasada al vacío, el aceite de oliva virgen extra, el
jamón ibérico de bellota, el chocolate negro, el pan integral 100%, el gazpacho
envasado, las legumbres de bote, el pescado en lata, la leche UHT, los yogures,
los lácteos fermentados y las bebidas vegetales sin azúcares añadidos, son
algunos ejemplos de este grupo de alimentos procesados.
<br></br>
Otra cosa totalmente diferente es lo que se conoce como alimentos
ultraprocesados, que son los que el movimiento Real Food fomenta evitar para
tener una dieta saludable. Son lo opuesto a la comida real que hemos definido
anteriormente y sus principales características son:
<br></br>
Se elaboran de forma industrial a partir de sustancias sintéticas o de otros
alimentos.
<br></br>
  <center><?= Html::img('@web/../img/realfood.png', ['alt' => '']) ?></center>
 <br></br>
Son el resultado de aplicar diferentes técnicas de procesamiento industrial que
hacen que estén listos para consumir y resulten más atractivos.
Su consumo tiene efectos negativos para la salud. Es decir, no tienen cabida
dentro de una dieta saludable.
<br></br>
Suelen tener más de cinco ingredientes entre los que figuran los aditivos, la sal,
las harinas refinadas, los azúcares añadidos y los aceites vegetales refinados.
Esta clase de productos, a pesar de poder aportar sustento energético, a medio
y largo plazo pueden perjudicar la salud debido a la mala calidad de sus
ingredientes. Entonces, ¿por qué se consumen los ultraprocesados? La mayoría
de veces es una cuestión de falta de tiempo, ante lo que el movimiento Real
Food plantea alternativas en forma de recetas rápidas para una dieta saludable.
<br></br>
Hay algunos productos como los zumos envasados, las carnes y los pescados
procesados que popularmente se perciben como saludables, pero nada más
lejos de la realidad tal y como acabamos de explicar. Otros alimentos
ultraprocesados, sin embargo, podemos intuir que no son muy recomendables,
aún así no conviene perderlos de vista. ¡Un repaso rápido! Estamos hablando
sobre todo de refrescos y lácteos azucarados, bebidas energéticas, la bollería
industrial, los panes refinados, las pizzas industriales, las galletas, los cereales
refinados, los dulces, las chucherías, los helados, los snacks salados y los
precocinados.
<br></br>
  <center><?= Html::img('@web/../img/cantabria.png', ['alt' => '']) ?></center>
<br></br>
En conclusión
<br></br>
Para poder identificar la Real Food de forma sencilla utiliza la regla de “menos
de 5 ingredientes” cuando mires el etiquetado de alimentos. Pero, más allá de
aprender a elegir únicamente alimentos procesados saludables, aplica el
sentido común para llevar una dieta saludable y apuesta siempre que sea
posible, por los productos frescos.
            </div>
        </div>

</div>
<br></br>
 <div class="footer">
        <p>Footer</p>
    </div>
    
     <style>
  
        .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: black;
        color: white;
        text-align: center;
        }
        </style>
</div>
