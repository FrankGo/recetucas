<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;


$this->title = 'Recetas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recetas-index">
    
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
            <?= Html::a('Create Recetas', ['create'], ['class' => 'btn ntn-success']) ?>
    </p>
    
    <?= GridView::widget([
        'dataProvider' =>  $dataProvider,
        'columns' =>  [
            
            'nombre',
            'duracion',
        ],
        
    ]); ?>
    
</div>
