<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Nuestras recetas';

?>

<div class="col-sm-6n col-md12">
    <div class="thumbnail noticias" style=max-height:900px;">
        <div class="caption">
            
            <h2><?= $model->nombre ?></h2>
            
            <hr class="my-4">
                <h4>Características de la receta</h4>
            <div class="btn-group btn-group-justifieds" style=margin:10px;">
                <?= "Categoria de la receta: " . $model->categorias->nombre ?>
            </div>
            
            <div class="btn-group btn-group-justifieds" style=margin:10px;">
                <?= "Dificultad de la receta: " . $model->dificultad['dificultad'] ?>
             </div>
            
             <hr class="my-4">
             
                <h4>Ingredientes de la receta:</h4>
             <div>
                 <div style=margin-left:10px;display:inline-block;background:#E9E9E9;margin-bottom:4px;padding:4px;border-radius:5px;" ><?= implode('</div><div style=margin-left:10px;display:inline-block;background:#E9E9E9;margin-bottom:4px;padding:4px;border-radius:5px;">', ArrayHelper::getColumn($model->ingredientes, 'nombre')) ?></div>
             
                 <hr class="my-4">
                <h4>Pasos de la receta:</h4>
             <div>
                 <div style=margin-left:10px;display:inline-block;background:#E9E9E9;margin-bottom:4px;padding:4px;border-radius:5px;" ><?= implode('</div><div style=margin-left:10px;display:inline-block;background:#E9E9E9;margin-bottom:4px;padding:4px;border-radius:5px;">', ArrayHelper::getColumn($model->pasos, 'descripcion')) ?></div>
             
   </div>
    
  </div>
</div>
        </div>
<br></br>
     <div class="footer">
        <p>Footer</p>
    </div>
    
     <style>
  
        .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: black;
        color: white;
        text-align: center;
        }
        </style>
</div>
