<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;

$this->title = "Nuestras recetas";

?>


<div class="well well-sm"><h2 style="text-aling: center; maxheigth: 50px;"><?=$titulo?></h2></div>
    
<p>
    <div class="row">
  
   
     <?= ListView::widget([
        'dataProvider' =>  $dataProvider,
        'itemView' =>  '_recetas',
        'layout'=>"{summary}\n{pager}\n{items}",
 
    ]); ?>
    
   
     <style>
  
        .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: black;
        color: white;
        text-align: center;
        }
        </style>
</div>
