<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Aceite de Oliva';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <center><h1><?= Html::encode($this->title) ?></h1></center>
    <br></br>
  <center><?= Html::img('@web/../img/aceite.png', ['alt' => '']) ?></center>
   <br></br>
     <div class="row">
            <div class="col-lg-12">
En las últimas décadas del siglo pasado, diversos estudios han comprobado las cualidades nutricionales del aceite de oliva y han confirmado que su consumo influye positivamente en la prevención de la aterosclerosis, protegiendo así frente a las enfermedades cardiovasculares. Pero esta es tan solo una de las numerosas virtudes del llamado oro líquido. Aquí te presentamos algunos de los beneficios que el aceite de oliva puede aportar a tu organismo:

Previene las enfermedades cardiovasculares: los ácidos grasos presentes en el aceite de oliva virgen, especialmente el ácido oleico, contribuyen a reducir los niveles de colesterol LDL (colesterol malo), mientras que aumentan los de colesterol HDL, o colesterol bueno, incrementan la vasodilatación arterial, mejorando la circulación sanguínea y disminuyendo la presión arterial.

Favorece la función digestiva y reduce la secreción ácida gástrica, protegiendo frente a las enfermedades gastrointestinales.

   <br></br>
<center><?= Html::img('@web/../img/aceite2.png', ['alt' => '']) ?></center>
       <br></br>

            </div>

Ayuda a combatir el estreñimiento, por tener un suave efecto laxante, y mejora la absorción intestinal de los nutrientes.

Disminuye la incidencia de complicaciones en los pacientes con diabetes mellitus tipo II. Un elevado consumo de grasa saturada conduce a sobrepeso y obesidad, importantes factores de riesgo para la aparición y empeoramiento de esta enfermedad, por eso las recomendaciones nutricionales para personas con diabetes tipo II, aunque individualizadas según las características del paciente, suelen incluir la dieta mediterránea y el consumo de ácidos grasos monoinsaturados, sobre todo ácido oleico.

Contribuye a una correcta mineralización de los huesos, y a su desarrollo. Es, pues, muy importante, que esté presente en la dieta de los niños durante el crecimiento, y también en la edad adulta para limitar la pérdida de calcio que se produce durante el envejecimiento, y que puede desembocar en patologías como la osteoporosis.

Desempeña un papel protector frente al estrés oxidativo celular por su elevado contenido en antioxidantes fenólicos, como la vitamina E.

Aumenta la longevidad, al reducir las muertes por enfermedades cardiovasculares y cáncer. Los resultados obtenidos en diversos estudios científicos han demostrado una menor incidencia de varios tipos de cáncer en países mediterráneos (los principales consumidores de aceite de oliva) en comparación con países del Norte de Europa y Estados Unidos. Es lo que ocurre, por ejemplo, con el cáncer de mama, relacionado con el consumo de grasa saturada de origen animal.

     <br></br>
<center><?= Html::img('@web/../img/aceite3.png', ['alt' => '']) ?></center>
       <br></br>

            </div>
</br>
          <center><?= Html::a('Recetas con Aceite de Oliva', ['recetas/recetasporingrediente','id' => 8], ['class' => 'btn btn-default']) ?></center>
    
</div>
<br></br>
     <div class="footer">
        <p>Footer</p>
    </div>
    
     <style>
  
        .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: black;
        color: white;
        text-align: center;
        }
        </style>
</div>
