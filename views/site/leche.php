<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = ''
        . 'Leche';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <center><h1><?= Html::encode($this->title) ?></h1></center>
    <br></br>
  <center><?= Html::img('@web/../img/cantabria.png', ['alt' => '']) ?></center>
   <br></br>
     <div class="row">
            <div class="col-lg-12">
A muchas personas les cuesta beber leche en la etapa adulta ya sea porque no les gusta el sabor o por intolerancia a la lactosa. Sin embargo, también existen personas que no lo hacen porque creen que la leche es mala para los adultos.

Existen varios mitos sobre la producción, distribución y efectos de la leche en el organismo que merecen ser aclarados.

Esencial en todas las fases de la vida: la leche hace bien a nuestra salud. La OMS (Organización Mundial de la Salud) recomienda tres porciones diarias de leche o derivados como el yogurt, el queso o las bebidas derivadas de la leche.

Alimento completo: la leche es un alimento completo, fuente de diferentes nutrientes esenciales como las proteínas, los minerales (fósforo y zinc) y calcio. También es responsable de la salud ósea.

Bueno para dormir: la leche también te puede ayudar a dormir. De hecho, el calcio estimula la relajación y el aminoácido triptófano aumenta la cantidad de serotonina en el cerebro, vital para el sueño.
<br></br>
<center><?= Html::img('@web/../img/leche1.png', ['alt' => '']) ?></center>
       <br></br>
Recomendada para actividades físicas: poca gente lo sabe pero la leche es una gran bebida hidratante. Consumirla tras hacer ejercicio beneficia a los músculos y a la hidratación.

Prevención del cáncer: ¿Quieres otro motivo para beber leche? diversos estudios apuntan a que la leche y sus nutrientes (como el calcio, la vitamina D, la lactoferrina y las proteínas del suero de leche) tienen efectos positivos sobre la reducción de posibilidad de contraer cáncer, especialmente el de colon.

Aumenta la inmunidad: este alimento es poderoso para aislarte de las enfermedades, además posee una gran acción antioxidante.


<br></br>
          <center><?= Html::a('Recetas con Leche', ['site/recetasporcategorias','id' => 7], ['class' => 'btn btn-default']) ?></center>
            </div>

</div>
<br></br>
     <div class="footer">
        <p>Footer</p>
    </div>
    
     <style>
  
        .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: black;
        color: white;
        text-align: center;
        }
        </style>
</div>
