<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Sobre Nosotros y la tierruca';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <center><h1><?= Html::encode($this->title) ?></h1></center>

     <div class="row">
            <div class="col-lg-12">
                </br>
La gastronomía de nuestra tierra se ve beneficiada gracias a la situación geográfica tan privilegiada que presenta. Rodeada por el mar Cantábricos y las montañas de la cordillera Cántabra, cuenta con una amplia variedad de ingredientes.  

Los pescados y mariscos que nos ofrece el mar y los ríos, que riegan y bañan hasta llegar a su destino nuestros frondosos bosques y campos donde se encuentran las hortalizas y legumbres de la huerta, y los pastos de una ganadería autóctona.
                </br>
                </br>
El marisco es uno de los protagonistas en las mesas de nuestros restaurantes y casas. Gracias a las frías y limpias aguas de las que procede posee una gran diversidad y calidad. Si bien puede conseguirse en toda la costa, es en la bahía de Santander donde se encuentran los mejores ejemplares. 

El vacuno es la carne cántabra por excelencia, donde destaca la de la vaca tudanca. No hay que olvidar que la feria ganadera más importante de España, la Feria Nacional de Ganados de Torrelavega, se celebra en esta región.

La caza también ofrece carne de gran calidad: venado, corzo y jabalí. El chon es un elemento clave para el cocido montañés, al que además se le añaden alubias, berzas, chorizo y morcilla de arroz.
               <br></br>
  <center><?= Html::img('@web/../img/cantabria1.png', ['alt' => '']) ?></center>
 <br></br>
Es indiscutible la excelencia de la leche cántabra, por lo que no puede extrañar que los derivados de la misma proliferen por toda la región: la mantequilla, el queso de nata, que puede encontrarse por toda la región; el queso picón en Tresviso y Bejes; quesos ahumados como los de Áliva o Pido; o los pequeños quesucos, realizados con la mezcla de leche de vaca y oveja. 

Además, de todos los productos derivados, que permiten realizar una repostería como los tradicionales sobaos y quesadas pasiegas. También es muy típico el hojaldre, que según la zona adopta diferentes nombres siendo de igual manera de una excelente calidad.

Por otro lado, la Miel de Liébana es una denominación de origen protegida (DOP) de miel propia de la comarca de Liébana.
                <br></br>
  <center><?= Html::img('@web/../img/cantabria2.png', ['alt' => '']) ?></center>
 <br></br>
La bebida más característica de esta región es el orujo, elaborado artesanalmente, y destilado gota a gota. Dicen los entendidos que el orujo es una bebida excelente para aliviar digestiones pesadas. Históricamente hubo gran producción de sidra y chacolí, que tras un importante declive se está recuperando en los últimos años. Cantabria cuenta actualmente con dos certificaciones de origen en vinos: Vino de la Tierra Costa de Cantabria y Vino de la Tierra de Liébana. 

            </div>
        </div>

</div>
</br>
    </br>

 <div class="footer">
        <p>Footer</p>
    </div>
    
     <style>
  
        .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: black;
        color: white;
        text-align: center;
        }
        </style>
</div>
