<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Objetivos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <center><h1><?= Html::encode($this->title) ?></h1></center>
    <br></br>
  <center><?= Html::img('@web/../img/cantabria.png', ['alt' => '']) ?></center>
   <br></br>
     <div class="row">
            <div class="col-lg-12">
Nuestro objetivo fundamental es, siguiendo los principios de Las Recetucas,
acercar la cocina a los usuarios para que puedan desarrollar platos asequibles
y fáciles pero con el toque de las nuevas técnicas de la restauración actual.
       <br></br>
Todos nuestras recetas cuentan con la parte de aprendizaje en la que de una
manera amena y práctica se elaboran las recetas que componen este espacio,
y posteriormente (detrás de la pantalla) se procede a degustar los platos
realizados en compañía de una copita de vino y la familia y los amigos, al fin y
al cabo estamos en Cantabria y eso siempre ha sido una de nuestras máximas,
compartir la gastronomía con los nuestros.
     <br></br>
<center><?= Html::img('@web/../img/realfood.png', ['alt' => '']) ?></center>
       <br></br>
Otro de los objetivos que se ha propuesto nuestro espacio culinario es
desmitificar la alta gastronomía española, mostrar que las grandes recetas de
este país también son asequibles, y queremos mostrarlo con deseo de
transmitir este conocimiento a la gente. Y para ello qué mejor que invitar a
algunos de nuestros colaboradores a que nos muestren algunas de sus
recetas.
      <br></br>
Estas recetas están pensadas para todos los públicos. Es decir, no es
necesario tener grandes conocimientos de cocina para poder seguirlos y
disfrutar de ellos. Se pretende que los usuarios pierdan el miedo a cocinar, a
enfrascarse en preparaciones más allá de lo precocinado y comida procesada.
            </div>

    
</div>
<br></br>
     <div class="footer">
        <p>Footer</p>
    </div>
    
     <style>
  
        .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: black;
        color: white;
        text-align: center;
        }
        </style>
</div>
