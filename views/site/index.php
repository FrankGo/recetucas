<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Las Recetucas';

?>

<div class="site-index">
    
     <?= Html::a(Html::img('@web/../img/ig.png'),Url::to('https://www.instagram.com/lasrecetucas', true))?>

    <div class="jumbotron">
   
      <center><?= Html::img('@web/../img/logorecetucas.png', ['alt' => '']) ?></center>
      
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <center><h2>Nuestro Nombre</h2></center>

                <p>Buscando utilizar la terminación "-uco" para denotar nuestros origenes y darle al nombre una connotación más entrañable y cariñosa. La gastronomía de nuestra tierra se ve beneficiada gracias a la situación geográfica tan privilegiada que presenta.</p>

                <center><p><a class="btn btn-default" href="@web/../index.php/site/about">Conoce más</a></p></center>
            </div>
            <div class="col-lg-4">
                <center><h2>Objetivos</h2></center>

                <p>De clara vocación tradicional, las recetucas es ante todo una página web abierta, moderna y dinámica, un espacio para crear, desarrollar y compartir nuevas tendencias y conocimientos que aporten un valor añadido al sector culinario.</p>

                <center><p><a class="btn btn-default" href="@web/../index.php/site/objetivos">Objetivos</a></p></center>
            </div>
            <div class="col-lg-4">
                <center><h2>Real Food</h2></center>

                <p>La alimentación se ha centrado en nutrientes y calorías, en lugar de alimentos, en lugar de comida real. Hablamos en términos de hidratos de carbono, grasas, proteínas, vitaminas, minerales… cuando todo eso no tiene nada que ver con la salud.</p>

                <center><p><a class="btn btn-default" href="@web/../index.php/site/realfood">Real Food</a></p></center>
            </div>
        </div>
        </br></br>
      <center><?= Html::a(Html::img('@web/../img/top3.png', ['alt' => 'Top3']),['site/top3'])?></center>
             
        
  </div><div class="body-content">
    <div class= "jumbotron">
    <h2><b>Recetas por Categorías<b/></h2>
    </div>
  <div class="d-flex justify-content-center">
   <div class="row">
    <div class="col-sm-3">
        <div class="thumbnail">
        <center><?= Html::img('@web/../img/postres.png', ['alt' => '']) ?></center>
        </br>
          <center><?= Html::a('Postres', ['site/recetasporcategorias','id' => 1], ['class' => 'btn btn-default']) ?></center>
<!--       <center><p><a href="/index.php/site/recetasporcategoria" class="btn btn-default" role="button">Postres</a></p></center>-->
        </div>
    </div>
       
    
     <div class="col-sm-3">
        <div class="thumbnail">
       <center><?= Html::img('@web/../img/arroces.png', ['alt' => '']) ?></center>
        </br>
          <center><?= Html::a('Arroces', ['site/recetasporcategorias','id' => 2], ['class' => 'btn btn-default']) ?></center>
<!--       <center><p><a href="#" class="btn btn-default" role="button">Arroces</a></p></center>-->
        </div>
    </div>
  
   
     <div class="col-sm-3">
        <div class="thumbnail">
        <center><?= Html::img('@web/../img/picoteo.png', ['alt' => '']) ?></center>
        </br>
           <center><?= Html::a('Picoteo', ['site/recetasporcategorias','id' => 3], ['class' => 'btn btn-default']) ?></center>
<!--       <center><p><a href="#" class="btn btn-default" role="button">Picoteo</a></p></center>-->
        </div>
    </div>
             
    
    <div class="col-sm-3">
        <div class="thumbnail">
        <center><?= Html::img('@web/../img/carnes.png', ['alt' => '']) ?></center>
        </br>
           <center><?= Html::a('Carnes', ['site/recetasporcategorias','id' => 4], ['class' => 'btn btn-default']) ?></center>
<!--       <center><p><a href="#" class="btn btn-default" role="button">Carnes</a></p></center>-->
        </div>
    </div>
    
     <div class="row">
    <div class="col-sm-3">
        <div class="thumbnail">
        <center><?= Html::img('@web/../img/cremas.png', ['alt' => '']) ?></center>
        </br>
          <center><?= Html::a('Cremas', ['site/recetasporcategorias','id' => 5], ['class' => 'btn btn-default']) ?></center>
<!--       <center><p><a href="#" class="btn btn-default" role="button">Cremas y Sopas</a></p></center>-->
        </div>
    </div>
       
    
    <div class="col-sm-3">
        <div class="thumbnail">
        <center><?= Html::img('@web/../img/ensaladas.png', ['alt' => '']) ?></center>
        </br>
          <center><?= Html::a('Ensaladas', ['site/recetasporcategorias','id' => 6], ['class' => 'btn btn-default']) ?></center>
<!--       <center><p><a href="#" class="btn btn-default" role="button">Ensaladas</a></p></center>-->
        </div>
    </div>
  
    
    <div class="col-sm-3">
        <div class="thumbnail">
        <center><?= Html::img('@web/../img/huevos.png', ['alt' => '']) ?></center>
        </br>
          <center><?= Html::a('Huevos', ['site/recetasporcategorias','id' => 7], ['class' => 'btn btn-default']) ?></center>
<!--       <center><p><a href="#" class="btn btn-default" role="button">Huevos</a></p></center>-->
        </div>
    </div>
             
    
    <div class="col-sm-3">
        <div class="thumbnail">
        <center><?= Html::img('@web/../img/pizzas.png', ['alt' => '']) ?></center>
        </br>
        <center><?= Html::a('Pasta y Pizzas', ['site/recetasporcategorias','id' => 8], ['class' => 'btn btn-default']) ?></center>
<!--       <center><p><a href="#" class="btn btn-default" role="button">Pizzas</a></p></center>-->
        </div>
    </div>
    
      <div class="row">
    <div class="col-sm-3">
        <div class="thumbnail">
       <center><?= Html::img('@web/../img/pescados.png', ['alt' => '']) ?></center>
        </br>
        <center><?= Html::a('Pescados', ['site/recetasporcategorias','id' => 9], ['class' => 'btn btn-default']) ?></center>
<!--       <center><p><a href="#" class="btn btn-default" role="button">Pescados y Mariscos</a></p></center>-->
        </div>
    </div>
       
   
    <div class="col-sm-3">
        <div class="thumbnail">
        <center><?= Html::img('@web/../img/potajes.png', ['alt' => '']) ?></center>
        </br>
        <center><?= Html::a('Potajes', ['site/recetasporcategorias','id' => 10], ['class' => 'btn btn-default']) ?></center>
<!--       <center><p><a href="#" class="btn btn-default" role="button">Potajes y Legumbres</a></p></center>-->
        </div>
    </div>
  
    
    <div class="col-sm-3">
        <div class="thumbnail">
        <center><?= Html::img('@web/../img/setas.png', ['alt' => '']) ?></center>
        </br>
        <center><?= Html::a('Setas', ['site/recetasporcategorias','id' => 11], ['class' => 'btn btn-default']) ?></center>
<!--       <center><p><a href="#" class="btn btn-default" role="button">Setas y Hongos</a></p></center>-->
        </div>
    </div>
             
   
    <div class="col-sm-3">
        <div class="thumbnail">
        <center><?= Html::img('@web/../img/verduras.png', ['alt' => '']) ?></center>
        </br>
        <center><?= Html::a('Verduras', ['site/recetasporcategorias','id' => 12], ['class' => 'btn btn-default']) ?></center>
        
<!--       <center><p><a href="#" class="btn btn-default" role="button">Verduras y Hortalizas</a></p></center>-->
        </div>
                <br>
        </br>

    </div>
          
<div class="col-sm-1">
         <center><?= Html::a(Html::img('@web/../img/promo1.png', ['alt' => '']),['site/mostrarrecetas'])?></center>
</div>
           </div><div class="body-content">
    <div class= "jumbotron">
    <h2><b>Noticias de Actualidad<b/></h2>
    </div>
      
  <center><?= Html::img('@web/../img/aceite1.png', ['alt' => '']) ?></center>
 <br></br>
                <h4>El aceite de oliva, conoce los beneficios del oro líquido.
                Es la piedra angular de la Dieta Mediterránea, el aceite no sólo destaca por sus
cualidades organolépticas, sino que, además, tiene multitud de ventajas para
nuestra salud. Conoce sus tipos y cómo sacarle el máximo partido en la cocina.</h4>

                <center><?= Html::a('Recetas con Aceite de Oliva', ['recetas/recetasporingrediente','ingrediente' => 8], ['class' => 'btn btn-default']) ?></center>

 <br></br>
  <center><?= Html::img('@web/../img/leche.jpg', ['alt' => '']) ?></center>
 <br></br>
                <h4>Esencial en todas las fases de la vida: la leche hace bien a nuestra salud. La
OMS (Organización Mundial de la Salud) recomienda tres porciones diarias de
leche o derivados como el yogurt, el queso o las bebidas derivadas de la leche.</h4>

                <center><?= Html::a('Recetas con Leche', ['recetas/recetasporingrediente','ingrediente' => 10], ['class' => 'btn btn-default']) ?></center>
                
 
 <br></br>
  <center><?= Html::img('@web/../img/setas1.png', ['alt' => '']) ?></center>
 <br></br>
                <h4>Más allá de ser ingrediente de algunos de los platos más nobles, las setas
tienen un alto valor nutricional y para la salud por las distinas sustancias y
componentes que aportan y por los efectos que tienen sobre nuestro
metabolismo.</h4>

                <center><?= Html::a('Recetas con Setas', ['recetas/recetasporingrediente','ingrediente' => 17], ['class' => 'btn btn-default']) ?></center>
            </div>
    
    <div class="footer">
        <p>Footer</p>
    </div>
    
     <style>
  
        .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: black;
        color: white;
        text-align: center;
        }
        </style>
</div>
