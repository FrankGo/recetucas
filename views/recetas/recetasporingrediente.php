<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\grid\HtmlPurificer;


$this->title = $title.$ingrediente;

$this->params['breadcrumbs'][] = $this->title;


?>
<div class="well well-sm"><h4 style="text-align: center;max-height: 50 px;"><?= $title.$ingrediente ?>

    <p>
            <?= Html::a('Create Ingrediente', ['create'], ['class' => 'btn ntn-success']) ?>
    </p>
    
    <div class="row"> 
    <?= ListView::widget([
        'dataProvider' =>  $dataProvider,
        'itemView' => '_recetasporingrediente',
        'layout' =>"\n{items}",
        
    ]); ?>
    
</div>
