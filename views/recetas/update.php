<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recetas */

$this->title = 'Cargar Recetas: ' . $model->id_recetas;
$this->params['breadcrumbs'][] = ['label' => 'Recetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_recetas, 'url' => ['view', 'id' => $model->id_recetas]];
$this->params['breadcrumbs'][] = 'Cargar';
?>
<div class="recetas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
