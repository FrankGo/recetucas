<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Comentan */

$this->title = $model->id_comentarios;
$this->params['breadcrumbs'][] = ['label' => 'Comentan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="comentan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Cargar', ['Update', 'id' => $model->id_comentarios], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['Delete', 'id' => $model->id_comentarios], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_comentarios',
            'id_usuarios',
            'id_recetas',
        ],
    ]) ?>

</div>
