<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Comentan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comentan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Comentan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
           /* ['class' => 'yii\grid\SerialColumn'],*/

            'id_comentarios',
            'id_usuarios',
            'id_recetas',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
