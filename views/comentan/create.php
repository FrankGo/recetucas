<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Comentan */

$this->title = 'Crear Comentan';
$this->params['breadcrumbs'][] = ['label' => 'Comentan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comentan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
