<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Comentan */

$this->title = 'Update Comentan: ' . $model->id_comentarios;
$this->params['breadcrumbs'][] = ['label' => 'Comentan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_comentarios, 'url' => ['view', 'id' => $model->id_comentarios]];
$this->params['breadcrumbs'][] = 'Cargar';
?>
<div class="comentan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
