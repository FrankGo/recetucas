<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ingredientes */

$this->title = 'Cargar Ingredientes: ' . $model->id_ingredientes;
$this->params['breadcrumbs'][] = ['label' => 'Ingredientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_ingredientes, 'url' => ['view', 'id' => $model->id_ingredientes]];
$this->params['breadcrumbs'][] = 'Cargar';
?>
<div class="ingredientes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
