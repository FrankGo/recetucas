<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ingredientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredientes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Ingredientes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
          /*  ['class' => 'yii\grid\SerialColumn'], */

            'id_ingredientes',
            'nombre',
            ['class' => 'yii\grid\ActionColumn'],
            ['class' => 'yii\grid\ActionColumn',
                 'header' =>'Recetas con este ingrediente',
                 'template' => '{recetas}',
                 'buttons' => [
                     'recetas' => function($url, $model) {
                        return Html::a('Ver recetas', ['recetas/recetasporingrediente', 'ingrediente' => $model->id_ingredientes], ['class' => 'btn btn-primary btn-md']);
                     },
                 ],
             ],
        ]
    ]); 
                     ?>


</div>
